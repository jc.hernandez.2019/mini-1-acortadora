
import webapp
from urllib.parse import unquote
import string
import random

FORM = """
<form action="" method="POST">
    <p>URL: <input type="text" name="url"></p>
    <p><input type="submit" value="SHORT"></p>
</form>
"""

PAGE = """
<html>
    <body>
        <h1>Enter the URL you want to short:</h1>
            <p>{form}</p>
        <h1>Stored URLS:</h1>
            <p>{urls}</p>
    </body>
</html>
"""

RESOURCE_NOT_FOUND = """
<html>
    <body>
        <h1>Resource not found.Check the shortened URL you have introduced.</h1>
    </body>
</html>
"""


class RandomShortApp(webapp.WebApp):

    def __init__(self, hostname, port):

        self.urls = {}
        super().__init__(hostname, port)

    def parse(self, request):

        data = {'method': request.split(' ', 2)[0],
                'resource': request.split(' ', 2)[1],
                'body': unquote(request.split('\r\n\r\n', 1)[1])}

        return data

    def process(self, data):

        if data['method'] == 'GET':
            http_code, html_body = self.get(data['resource'])
        elif data['method'] == 'POST':
            http_code, html_body = self.post(data['body'])

        return http_code, html_body

    def get(self, resource):

        if resource == '/':

            html_body = PAGE.format(urls=self.urls, form=FORM)
            http_code = '200 OK'

        elif resource in self.urls.keys():

            html_body = '<meta http-equiv="refresh" content="0;url=' + self.urls[resource] + '">'
            http_code = '301 Moved Permanently'

        else:

            html_body = RESOURCE_NOT_FOUND
            http_code = '404 Not Found'

        return http_code, html_body

    def post(self, body):

        url = unquote(body.split("=")[1])
        if not url.startswith('http') or not url.startswith('https'):
            url = 'https://' + url

        if url not in self.urls.values():
            shortened = ''.join(random.choices(string.ascii_lowercase + string.digits, k=5))
            resource = '/' + shortened
            self.urls[resource] = url

        html_body = PAGE.format(urls=self.urls, form=FORM)
        http_code = '200 OK'

        return http_code, html_body


if __name__ == "__main__":
    testRandomShortApp = RandomShortApp("", 1234)
